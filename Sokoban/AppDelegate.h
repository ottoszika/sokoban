//
//  AppDelegate.h
//  Sokoban
//
//  Created by totti93 on 12/01/16.
//  Copyright © 2016 ottoszika. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

