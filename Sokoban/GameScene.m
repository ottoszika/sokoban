//
//  GameScene.m
//  Sokoban
//
//  Created by totti93 on 12/01/16.
//  Copyright (c) 2016 ottoszika. All rights reserved.
//

#import "GameScene.h"

@implementation GameScene

// Drag error epsilon
int maxDragError = 50;

- (void)didMoveToView:(SKView *)view {
    self.backgroundColor = [SKColor colorWithRed:101.0f / 255 green:159.0f / 255 blue:62.0f / 255 alpha:1.0f];
    
    NSString *url = [[NSUserDefaults standardUserDefaults] objectForKey:@"url"];
    
    _game = [[Game alloc] initWithSKScene:self url:url];
    
    [_game render];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    // More than one touch? (neah...)
    if ([touches count] > 1) {
        return ;
    }
    
    // Get touch and location
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    
    // Note touch start position
    _touchStartX = location.x;
    _touchStartY = location.y;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {

    // Get touch and location
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    
    // Note touch end position
    _touchEndX = location.x;
    _touchEndY = location.y;
    
    if (abs(_touchStartX - _touchEndX) < maxDragError && abs(_touchStartY - _touchEndY) > maxDragError) { // Up-Down
        
        // Up
        if (_touchStartY > _touchEndY) {
            [_game move:MOVE_DOWN];
        }
        
        // Down
        if (_touchStartY < _touchEndY) {
            [_game move:MOVE_UP];
        }

    } else if (abs(_touchStartY - _touchEndY) < maxDragError && abs(_touchStartX - _touchEndX) > maxDragError) { // Left-Right
        
        // Left
        if (_touchStartX > _touchEndX) {
            [_game move:MOVE_LEFT];
        }
        
        // Down
        if (_touchStartX < _touchEndX) {
            [_game move:MOVE_RIGHT];
        }

    }
}

- (void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
}

@end
