//
//  GameScene.h
//  Sokoban
//

//  Copyright (c) 2016 ottoszika. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "Game.h"

@interface GameScene : SKScene

@property (nonatomic) Game *game;                       // Game logic and renderer object
@property (nonatomic) int touchStartX, touchStartY;     // Touch start position
@property (nonatomic) int touchEndX, touchEndY;         // Touch end position

@end
