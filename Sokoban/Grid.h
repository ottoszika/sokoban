//
//  Grid.h
//  Sokoban
//
//  Created by totti93 on 12/01/16.
//  Copyright © 2016 ottoszika. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameElement.h"


// A grid system for the game
// The game will use this coordinate system to draw things
// Each grid element will be a map spritesheet
@interface Grid : NSObject

@property (nonatomic) NSMutableArray *matrix;                                       // Matrix to store elements
@property (nonatomic, readonly) NSMutableArray *matrixCopy;                         // Matrix to store (ca't be modified)
@property (nonatomic) int width, height;                                            // Grid size
@property (nonatomic) int screenWidth, screenHeight;                                // Screen size
@property (nonatomic) int gridWidth, gridHeight;                                    // Grid dimensions
@property (nonatomic) int dropZoneCount;                                            // How many dropzones are


#pragma mark - Constructors
- (id)init;                                                                         // Default constructor
- (id)initWithSize:(int)width height:(int)height;                                   // Constructor to initialize width / height
- (id)initWithJSON:(NSData *)data;                                                  // Init map from JSON map


#pragma mark - Actions
- (void)addGameElement:(GameElement *)gameElement x:(int)x y:(int)y;                // Add sprite node to grid
- (void)draw:(SKScene *)scene;                                                      // Draw all on scene
- (void)move:(int)fromX fromY:(int)fromY toX:(int)toX toY:(int)toY;                 // Move an element from one place to another


#pragma mark - Getters
- (GameElement *)getElementAt:(int)x y:(int)y;                                      // Get game element at x, y
- (BOOL) areBoxesOverDropzones;                                                     // Is it solved?


#pragma mark - Setters
- (void)setScreenSize:(int)screenWidth screenHeight:(int)screenHeight;              // Set screen height


#pragma mark - Overrides
- (NSString *)description;                                                          // Convert grid to string

@end
