//
//  LevelLoader.h
//  Sokoban
//
//  Created by totti93 on 20/01/16.
//  Copyright © 2016 ottoszika. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LevelLoader : NSObject

+(NSData *)loadFromURL:(NSString *)url;                             // Load level data from URL

@end
