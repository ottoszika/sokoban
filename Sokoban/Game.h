//
//  Game.h
//  Sokoban
//
//  Created by totti93 on 20/01/16.
//  Copyright © 2016 ottoszika. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Grid.h"

#define MOVE_UP                 1
#define MOVE_RIGHT              2
#define MOVE_DOWN               3
#define MOVE_LEFT               4

#define PLAYER_ANIMATION_UP     6
#define PLAYER_ANIMATION_DOWN   7
#define PLAYER_ANIMATION_LEFT   8
#define PLAYER_ANIMATION_RIGHT  9

@interface Game : NSObject

@property (nonatomic) SKScene *scene;                               // Scene

@property (nonatomic) Grid *grid;                                   // Grid system
@property (nonatomic) int playerX, playerY;                         // Player position

+ (NSDictionary *)playerAnimations;                                 // Player animations dictionary

- (id)initWithSKScene:(SKScene *)scene url:(NSString *)url;         // Init with scene
- (void)render;                                                     // Render game
- (void)move:(int)direction;                                        // Move player

@end
