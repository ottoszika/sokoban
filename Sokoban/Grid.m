//
//  Grid.m
//  Sokoban
//
//  Created by totti93 on 12/01/16.
//  Copyright © 2016 ottoszika. All rights reserved.
//

#import "Grid.h"


// Implementing Grid interface
@implementation Grid


// Implement default constructor
- (id)init {
    
    // Init a matrix
    _matrix = [[NSMutableArray alloc] init];
    
    return self;
}

// Implement constructor
- (id)initWithSize:(int)width height:(int)height {
    
    // Calling default constructor
    self = [self init];
    
    // If cannot initialize => return nil
    if (!self) {
        return nil;
    }
    
    // Initialize width and height
    _width = width;
    _height = height;
    
    
    // Initialize matrix with nulls
    for (int i = 0; i < self.height; i++) {
        
        // Create line and add objects to it
        NSMutableArray* line = [[NSMutableArray alloc] init];
        for (int j = 0; j < self.width; j++) {
            
            // Add sprite null to line
            [line addObject:[NSNull null]];
        }
        
        // Add line to each column
        [_matrix addObject:line];
    }
    
    return self;
}

// Implement JSON based constructor
- (id)initWithJSON:(NSData *)data {
    
    // Object for handling JSON errors
    NSError *error = nil;
    
    // Create a dictionary from JSON
    NSDictionary *map = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    // Check for error
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Cannot read JSON from server!"
                                                       delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil, nil];
        
        [alert show];
    }
    
    // Getting map size
    long width = [map[@"size"][@"width"] integerValue];
    long height = [map[@"size"][@"height"] integerValue];
    
    
    // Initalize map
    self = [self initWithSize:(int)width height:(int)height];
    
    // Creating objects
    for (int i = 0; i < [map[@"map"] count]; i++) {
        
        // Get map element (game element description from JSON)
        NSDictionary *mapElement = map[@"map"][i];
        
        // Getting fields
        NSNumber *type = [NSNumber numberWithLong:[mapElement[@"type"] integerValue]];
        long x = [mapElement[@"x"] integerValue];
        long y = [mapElement[@"y"] integerValue];
        
        // Create a new game element
        GameElement *gameElement = [[GameElement alloc] initWithType:type];
        
        // Add element in grid
        [self addGameElement:gameElement x:(int)x y:(int)y];
    }
    
    // Clone
    _matrixCopy = [[NSMutableArray alloc] initWithArray:_matrix copyItems:YES];
    
    return self;
}

// Add a GameElement type object
- (void)addGameElement:(GameElement *)gameElement x:(int)x y:(int)y {
    
    // Check for index out of range
    if (x < 0 || x >= _width || y < 0 || y >= _height) {
        return ;
    }
    
    // Increment dropzone count if needed
    if ([[gameElement type] isEqual:[NSNumber numberWithInt:DROPZONE_ELEMENT]]) {
        self.dropZoneCount++;
    }
    
    // Set object in matrix
    [[_matrix objectAtIndex:y] setObject:gameElement atIndexedSubscript:x];
}

// Convert to string
- (NSString *)description {
    
    // Instantiate a mutable string
    NSMutableString *objectDescription = [[NSMutableString alloc] init];
    
    // Build up a string with game element types
    for (int i = 0; i < _height; i++) {
        for (int j = 0; j < _width; j++) {
            
            // Get game element from matrix
            GameElement *gameElement = [[_matrix objectAtIndex:i] objectAtIndex:j];
            
            // Add type to string
            NSNumber *type = ![gameElement isKindOfClass:[NSNull class]] ? [gameElement type] : 0;
            [objectDescription appendString:[NSString stringWithFormat:@"%d", [type intValue]]];
        }
        
        // Add a new line to string
        [objectDescription appendString:@"\n"];
    }
    
    return objectDescription;
}

// Set screen size
- (void)setScreenSize:(int)screenWidth screenHeight:(int)screenHeight {
    _screenWidth = screenWidth;
    _screenHeight = screenHeight;
}

// Draw grid on scene
- (void)draw:(SKScene *)scene {
    _gridWidth = scene.size.width / _width;
    _gridHeight = scene.size.height / _height;
    
    for (int i = 0; i < _height; i++) {
        for (int j = 0; j < _width; j++) {
            GameElement *gameElement = [[_matrix objectAtIndex:i] objectAtIndex:j];
            
            if ([gameElement isKindOfClass:[NSNull class]]) {
                continue;
            }
            
            // Getting sprite from each game element and position / scale it
            SKSpriteNode *sprite = [[[_matrix objectAtIndex:i] objectAtIndex:j] getSpriteNode];
            sprite.position = CGPointMake(j * _gridWidth, i * _gridHeight);
            sprite.size = CGSizeMake(_gridWidth, _gridHeight);
            
            // Position of dropzone will be negative 1
            if ([[gameElement type] isEqual:[NSNumber numberWithInt:DROPZONE_ELEMENT]]) {
                sprite.zPosition = -1;
            }
            
            // Add to scene
            [scene addChild:sprite];
        }
    }
}

// Move game element
-(void)move:(int)fromX fromY:(int)fromY toX:(int)toX toY:(int)toY {
    
    // Get game element at `from` position
    GameElement *fromGameElement = [[_matrix objectAtIndex:fromX] objectAtIndex:fromY];
    
    // Set element at `to` position
    [[_matrix objectAtIndex:toX] setObject:fromGameElement atIndexedSubscript:toY];
    
    // Add null to `from` position
    [[_matrix objectAtIndex:fromX] setObject:[NSNull alloc] atIndexedSubscript:fromY];
}

// Get game element at
- (GameElement *)getElementAt:(int)x y:(int)y {
    return [[_matrix objectAtIndex:x] objectAtIndex:y];
}

- (BOOL)areBoxesOverDropzones {
    
    // Counter for deposited boxes
    int cnt = 0;
    
    // Check the two arrays
    for (int i = 0; i < _height; i++) {
        for (int j = 0; j < _width; j++) {
            
            // Get current elements from the original and copy matrix
            GameElement *elementNormal = [[_matrix objectAtIndex:i] objectAtIndex:j];
            GameElement *elementCopy = [[_matrixCopy objectAtIndex:i] objectAtIndex:j];
            
            // Do not continue with these values if any of them is nil
            if ([elementNormal isKindOfClass:[NSNull class]] ||
                [elementCopy isKindOfClass:[NSNull class]]) {
                
                continue;
            }
            
            // Count deposited boxes
            if ([[elementCopy type] isEqual:[NSNumber numberWithInt:DROPZONE_ELEMENT]] &&
                [[elementNormal type] isEqual:[NSNumber numberWithInt:CRATE_ELEMENT]]) {
                
                cnt++;
            }
        }
    }
    
    // BOOL - Are all boxes deposited?
    return [self dropZoneCount] == cnt;
}

@end
