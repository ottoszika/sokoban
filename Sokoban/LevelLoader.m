//
//  LevelLoader.m
//  Sokoban
//
//  Created by totti93 on 20/01/16.
//  Copyright © 2016 ottoszika. All rights reserved.
//

#import "LevelLoader.h"
#import <UIKit/UIKit.h>

@implementation LevelLoader

// Implement loader from URL
+ (NSData *)loadFromURL:(NSString *)url {

    // Error and response pointers
    NSError *err;
    NSURLResponse *response;
    
    // Prepare a GET request
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL  URLWithString:url]];
    [request setHTTPMethod:@"GET"];
    
    // Make HTTP request
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    
    
    if (err) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Cannot read JSON from server!"
                                                   delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil, nil];
        [alert show];
        
        return nil;
    }
    
    return data;
}

@end
