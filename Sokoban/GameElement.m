//
//  GameElement.m
//  Sokoban
//
//  Created by totti93 on 12/01/16.
//  Copyright © 2016 ottoszika. All rights reserved.
//

#import "GameElement.h"


// Implementating game element interface
@implementation GameElement


// Return an image name dictionary
+ (NSDictionary *)imageNames {
    
    // Create only one instance
    static NSDictionary *imageNamesDict = nil;
    
    // If no other instances were created...
    if (!imageNamesDict) {
        
        // Define image mapping
        imageNamesDict = @{
                            @WALL_ELEMENT:              @"e_wall",
                            @CRATE_ELEMENT:             @"e_crate",
                            @DROPZONE_ELEMENT:          @"e_dropzone",
                            @PLAYER_ELEMENT:            @"e_player"
                        };
    }
    
    return imageNamesDict;
}

// Implement constructor
- (id)initWithType:(NSNumber *)type {
    _type = type;
    return self;
}

// Get sprite node of game element
- (SKSpriteNode *)getSpriteNode {
    
    // Create a new sprite node if there was not initialized
    if (!_spriteNode) {
        _spriteNode = [[SKSpriteNode alloc] initWithImageNamed:[[[self class] imageNames] objectForKey:_type]];
    }
    
    return _spriteNode;
}

@end
