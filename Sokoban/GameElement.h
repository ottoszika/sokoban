//
//  GameElement.h
//  Sokoban
//
//  Created by totti93 on 12/01/16.
//  Copyright © 2016 ottoszika. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>


// Some constants
#define BLANK_ELEMENT       0
#define WALL_ELEMENT        1
#define FREEZONE_ELEMENT    2
#define CRATE_ELEMENT       3
#define DROPZONE_ELEMENT    4
#define PLAYER_ELEMENT      5


// Describe a single game element
// It can be a blank zone, a wall, a brick, a walkable free zone, a crate, a dropzone, or a plyer
// Each can be represented as a sprite node
@interface GameElement : NSObject

@property (nonatomic) NSNumber *type;                                   // The type of element
@property (nonatomic) SKSpriteNode *spriteNode;                         // The sprite node of element

+ (NSDictionary *)imageNames;                                           // The images name dictionary

#pragma mark - Constructors
- (id)initWithType:(NSNumber *)type;                                    // Initialize Game element with type

# pragma mark - Getters
- (SKSpriteNode *)getSpriteNode;                                        // Get the sprite node for the current type

@end
