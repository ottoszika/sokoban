//
//  Game.m
//  Sokoban
//
//  Created by totti93 on 20/01/16.
//  Copyright © 2016 ottoszika. All rights reserved.
//

#import "Game.h"
#import "LevelLoader.h"
#import "GameElement.h"

@implementation Game

// Player animation getter
+ (NSDictionary *)playerAnimations {
    
    // Create only one instance
    static NSDictionary *playerAnimations = nil;
    
    // If no other instances were created...
    if (!playerAnimations) {
        
        // Define anomation mapping
        playerAnimations = @{
            @PLAYER_ANIMATION_UP:       @[
                                          [SKTexture textureWithImageNamed:@"a_player_up_00"],
                                          [SKTexture textureWithImageNamed:@"a_player_up_01"],
                                          [SKTexture textureWithImageNamed:@"a_player_up_02"]
                                        ],
            @PLAYER_ANIMATION_DOWN:     @[
                                          [SKTexture textureWithImageNamed:@"a_player_down_00"],
                                          [SKTexture textureWithImageNamed:@"a_player_down_01"],
                                          [SKTexture textureWithImageNamed:@"a_player_down_02"]
                                        ],
            @PLAYER_ANIMATION_LEFT:     @[
                                          [SKTexture textureWithImageNamed:@"a_player_left_00"],
                                          [SKTexture textureWithImageNamed:@"a_player_left_01"]
                                        ],
            @PLAYER_ANIMATION_RIGHT:    @[
                                          [SKTexture textureWithImageNamed:@"a_player_right_00"],
                                          [SKTexture textureWithImageNamed:@"a_player_right_01"]
                                        ]
        };
    }
    
    return playerAnimations;
}

// Implementing constructor
- (id)initWithSKScene:(SKScene *)scene url:(NSString *)url {
    
    // Load grid from URL
    NSData *mapData = [LevelLoader loadFromURL:url];
    _grid = [[Grid alloc] initWithJSON:mapData];
    
    // Set property
    _scene = scene;
    
    // Getting player position
    for (int i = 0; i < [_grid height]; i++) {
        for (int j = 0; j < [_grid width]; j++) {
            
            // Get game element from (i, j)
            GameElement *gameElement = [_grid getElementAt:i y:j];
            
            // Check if there is an element and is player
            if (![gameElement isKindOfClass:[NSNull class]] &&
                [[gameElement type] isEqual:[NSNumber numberWithInt:PLAYER_ELEMENT]]) {
                
                // Set player position in Game
                _playerX = i;
                _playerY = j;
                
                // Jump out from nested loops
                goto ready;
            }
        }
    }
    
ready:
    return self;
}

// Render
- (void)render {
    [_grid draw:_scene];
}

// Move player
- (void)move:(int)direction {
    
    // Get player element and sprite node
    GameElement *player = [_grid getElementAt:_playerX y:_playerY];
    SKSpriteNode *playerNode = [player getSpriteNode];;
    
    // If player is near a crate and want to push it we will initialize it
    SKSpriteNode *crateNode = nil;
    
    // Animation for player movement
    NSArray *animation = nil;
    
    // Declare an action
    SKAction *action;
    
    // Run an animation
    switch (direction) {
        case MOVE_DOWN:
            animation = [[Game playerAnimations] objectForKey:[NSNumber numberWithInt:PLAYER_ANIMATION_DOWN]];
            break;
            
        case MOVE_UP:
            animation = [[Game playerAnimations] objectForKey:[NSNumber numberWithInt:PLAYER_ANIMATION_UP]];
            break;
            
        case MOVE_LEFT:
            animation = [[Game playerAnimations] objectForKey:[NSNumber numberWithInt:PLAYER_ANIMATION_LEFT]];
            break;
            
        case MOVE_RIGHT:
            animation = [[Game playerAnimations] objectForKey:[NSNumber numberWithInt:PLAYER_ANIMATION_RIGHT]];
            break;
    }
    
    // Create a walk action
    SKAction *walk = [SKAction animateWithTextures:animation timePerFrame:0.08];
    [playerNode runAction:walk];

    
    // Check if movement is possible
    if (direction == MOVE_LEFT) {
        
        // Get nearby element (left side)
        GameElement *near = [_grid getElementAt:_playerX y:_playerY - 1];
        
        // Player can go through freezone and dropzone
        if (![near isKindOfClass:[NSNull class]] && ![[near type] isEqual:[NSNumber numberWithInt:DROPZONE_ELEMENT]]) {
            
            // Check if near is crate
            if ([[near type] isEqual:[NSNumber numberWithInt:CRATE_ELEMENT]]) {
                
                // Getting element near crate
                GameElement *nearCrate = [_grid getElementAt:_playerX y:_playerY - 2];
                
                // Crate can be moved only in unallocated zone ore dropzone
                if ([nearCrate isEqual:[NSNull null]] || [[nearCrate type] isEqual:[NSNumber numberWithInt:DROPZONE_ELEMENT]]) {
                    crateNode = [near getSpriteNode];
                    [_grid move:_playerX fromY:_playerY - 1 toX:_playerX toY:_playerY - 2];
                } else {    // Invalid movement with crate
                    return ;
                }
            } else {    // Invalid movement with player
                return ;
            }
        }
        
        // Change player position in grid
        [_grid move:_playerX fromY:_playerY toX:_playerX toY:_playerY - 1];
        _playerY--;
        
    } else if (direction == MOVE_RIGHT) {
        
        // Get nearby element (right side)
        GameElement *near = [_grid getElementAt:_playerX y:_playerY + 1];
        
        // Player can go through freezone and dropzone
        if (![near isKindOfClass:[NSNull class]] && ![[near type] isEqual:[NSNumber numberWithInt:DROPZONE_ELEMENT]]) {
            
            // Check if near is crate
            if ([[near type] isEqual:[NSNumber numberWithInt:CRATE_ELEMENT]]) {
                
                // Getting element near crate
                GameElement *nearCrate = [_grid getElementAt:_playerX y:_playerY + 2];
                
                // Crate can be moved only in unallocated zone ore dropzone
                if ([nearCrate isEqual:[NSNull null]] || [[nearCrate type] isEqual:[NSNumber numberWithInt:DROPZONE_ELEMENT]]) {
                    crateNode = [near getSpriteNode];
                    [_grid move:_playerX fromY:_playerY + 1 toX:_playerX toY:_playerY + 2];
                } else {    // Invalid movement with crate
                    return ;
                }
            } else {        // Invalid movement with player
                return ;
            }
        }

        
        // Change player position in grid
        [_grid move:_playerX fromY:_playerY toX:_playerX toY:_playerY + 1];
        _playerY++;
        
    } else if (direction == MOVE_DOWN) {
        
        // Get nearby element (up side)
        GameElement *near = [_grid getElementAt:_playerX - 1 y:_playerY];
        
        // Player can go through freezone and dropzone
        if (![near isKindOfClass:[NSNull class]] && ![[near type] isEqual:[NSNumber numberWithInt:DROPZONE_ELEMENT]]) {
            
            // Check if near is crate
            if ([[near type] isEqual:[NSNumber numberWithInt:CRATE_ELEMENT]]) {
                
                // Getting element near crate
                GameElement *nearCrate = [_grid getElementAt:_playerX - 2 y:_playerY];
                
                // Crate can be moved only in unallocated zone ore dropzone
                if ([nearCrate isEqual:[NSNull null]] || [[nearCrate type] isEqual:[NSNumber numberWithInt:DROPZONE_ELEMENT]]) {
                    crateNode = [near getSpriteNode];
                    [_grid move:_playerX - 1 fromY:_playerY toX:_playerX - 2 toY:_playerY];
                } else {    // Invalid movement with crate
                    return ;
                }
            } else {    // Invalid movement with player
                return ;
            }
        }
        
        // Change player position in grid
        [_grid move:_playerX fromY:_playerY toX:_playerX - 1 toY:_playerY];
        _playerX--;
        
    } else if (direction == MOVE_UP) {
        
        // Get nearby element (up side)
        GameElement *near = [_grid getElementAt:_playerX + 1 y:_playerY];
        
        // Player can go through freezone and dropzone
        if (![near isKindOfClass:[NSNull class]] && ![[near type] isEqual:[NSNumber numberWithInt:DROPZONE_ELEMENT]]) {
            
            // Check if near is crate
            if ([[near type] isEqual:[NSNumber numberWithInt:CRATE_ELEMENT]]) {
                
                // Getting element near crate
                GameElement *nearCrate = [_grid getElementAt:_playerX + 2 y:_playerY];
                
                // Crate can be moved only in unallocated zone ore dropzone
                if ([nearCrate isEqual:[NSNull null]] || [[nearCrate type] isEqual:[NSNumber numberWithInt:DROPZONE_ELEMENT]]) {
                    crateNode = [near getSpriteNode];
                    [_grid move:_playerX + 1 fromY:_playerY toX:_playerX + 2 toY:_playerY];
                } else {    // Invalid movement with crate
                    return ;
                }
            } else {    // Invalid movement with player
                return ;
            }
        }
        
        // Change player position in grid
        [_grid move:_playerX fromY:_playerY toX:_playerX + 1 toY:_playerY];
        _playerX++;
        
    }
    
    // Create an action by direction
    switch (direction) {
        case MOVE_DOWN:
            action = [SKAction moveByX:0 y:-[_grid gridHeight] duration:0.2];
            break;
            
        case MOVE_UP:
            action = [SKAction moveByX:0 y:[_grid gridHeight] duration:0.2];
            break;
            
        case MOVE_LEFT:
            action = [SKAction moveByX:-[_grid gridWidth] y:0 duration:0.2];
            break;

        case MOVE_RIGHT:
            action = [SKAction moveByX:[_grid gridWidth] y:0 duration:0.2];
            break;
    }
    
    // Execute action
    [playerNode runAction:action completion:^{
        
        // Check for winning
        BOOL win = [_grid areBoxesOverDropzones];
        if (win) {
            
            // Show a message box
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congratulations" message:@"You finished this level!"
                                                           delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];

    // Check if we have to move crate
    if (crateNode != nil) {
        [crateNode runAction:action];
    }
}

@end
