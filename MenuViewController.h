//
//  MenuViewController.h
//  Sokoban
//
//  Created by totti93 on 21/01/16.
//  Copyright © 2016 ottoszika. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *urlField;

@end
